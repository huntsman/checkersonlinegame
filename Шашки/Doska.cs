﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Шашки
{
    public class Doska
    {
        public PaintBox m_paintbox;
        public Kletka[,] Klet_list;
        //int gamewindow;
        public Point gamewindow_p;
        Image Black_p;
        Image White_p;
        Image Black_kp;
        Image White_kp;
        Image BG;
        public Point KletSize;
        public BaseColor PlayerColor;

        public Doska(PaintBox pb)
        {
            m_paintbox = pb;
            Klet_list = new Kletka[8,8];
            gamewindow_p.X = 120;
            gamewindow_p.Y = 20;
            KletSize = new Point(36,36);
            PlayerColor = new BaseColor();
            ChangePlayerNum = CPn;
            ChangeGameBl = CPn;
            ChangeStepp = stepp;
            CReBuild = ReBuild;
        }

        public void LoadRes()
        {

            Black_p = Image.FromFile("./Data/Black_p.png");
            White_p = Image.FromFile("./Data/White_p.png");
            Black_kp = Image.FromFile("./Data/Black_dp.png");
            White_kp = Image.FromFile("./Data/White_dp.png");
            BG = Image.FromFile("./Data/BG.png");
        }

        public ChangePn ChangePlayerNum;
        public delegate void ChangePn(BaseColor bt);
        private void CPn(BaseColor bt)
        {
            PlayerColor = bt;
        }

        public ChangeGb ChangeGameBl;
        public delegate void ChangeGb(bool bt);
        private void CPn(bool bt)
        {
            m_paintbox.Enabled = bt;
        }

        public void Die_C(Point checker)
        {
            Point temp = Klet_list[checker.X, checker.Y].coord;
            Klet_list[checker.X, checker.Y].Pawn = null;
            Klet_list[checker.X, checker.Y].coord = temp;
        }

        public ChangeSp ChangeStepp;
        public delegate void ChangeSp(Point checker, Point checkerstepp);
        private void stepp(Point checker, Point checkerstepp)
        {
            //Point tpoint = Klet_list[checkerstepp.X, checkerstepp.Y].coord;
            Pawn tempp = Klet_list[checker.X, checker.Y].Pawn;
            Klet_list[checker.X, checker.Y].Pawn = null;
            Klet_list[checkerstepp.X, checkerstepp.Y].Pawn = tempp;


            //Klet_list[checker.X, checker.Y].coord = Klet_list[checkerstepp.X, checkerstepp.Y].coord;
            //Klet_list[checkerstepp.X, checkerstepp.Y].coord = tpoint;

            //Klet_list[checker.X, checker.Y].coord = checkerstepp;
            //Klet_list[checker.X, checker.Y].coord.X = checkerstepp.X;
            //Klet_list[checker.X, checker.Y].coord.Y = checkerstepp.Y;
            ReBuild();
        }


        public void InvertAll()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int ii = 0; ii < 4; ii++)
                {
                    Pawn pw = Klet_list[i, ii].Pawn;
                    Klet_list[i, ii].Pawn = Klet_list[7-i, 7 - ii].Pawn;
                    Klet_list[7-i, 7 - ii].Pawn = pw;

                }
            }
        }

        public void InvertStep(ref Point pin, ref Point pto)
        {
            pin.X = 7 - pin.X;
            pin.Y = 7 - pin.Y;
            pto.X = 7 - pto.X;
            pto.Y = 7 - pto.Y;
        }

        public void InvertPoint(ref Point pin)
        {
            pin.X = 7 - pin.X;
            pin.Y = 7 - pin.Y;
        }


        public rb CReBuild;
        public delegate void rb();
        private void ReBuild()
        {
            m_paintbox.Clear();
            m_paintbox.PaintImage(BG, new Point(0, 0));
            for (int i = 0; i < 8; i++)
            {
                for (int ii = 0; ii < 8; ii++)
                {
                    if (Klet_list[i, ii].Pawn != null)
                    {

                        if (Klet_list[i, ii].Pawn.Color == BaseColor.black)
                        {
                            if (Klet_list[i, ii].Pawn.isKing)
                                m_paintbox.PaintImage(Black_kp, Klet_list[i, ii].coord);
                            else
                                m_paintbox.PaintImage(Black_p, Klet_list[i, ii].coord);
                        }
                        else
                        {
                            if (Klet_list[i, ii].Pawn.isKing)
                                m_paintbox.PaintImage(White_kp, Klet_list[i, ii].coord);
                            else
                                m_paintbox.PaintImage(White_p, Klet_list[i, ii].coord);
                        }
                    }

                }
            }
        }



    }


    public class Kletka
    {
        public BaseColor Color;
        public Pawn Pawn;
        public Point coord;
    }

    public class Pawn
    {
        public BaseColor Color;
        public bool isKing;
    }


    public enum BaseColor
    {
        white,
        black,
    }
}
