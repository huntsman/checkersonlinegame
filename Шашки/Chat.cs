﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Шашки
{
    public partial class Chat : UserControl
    {
        public delegate void Send(string message);
        public event Send SendMsg;
        public Chat()
        {
            InitializeComponent();
            AddMesage = AddM;
        }


        public NewMesage AddMesage;
        public delegate void NewMesage(string msg, string who);
        private void AddM(string msg, string who)
        {
            ChatBox.Text += who + ">" + msg + "\r\n";
            ChatBox.SelectionStart = ChatBox.Text.Length;
            ChatBox.ScrollToCaret();
            SendBox.Text = "";
        }

        private void Send_Click(object sender, EventArgs e)
        {
            if (SendBox.Text.Length != 0)
                SendMsg(SendBox.Text);
        }
    }
}
