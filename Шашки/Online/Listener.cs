﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Net;
using System.IO;
using System.Net.Sockets;

namespace Шашки.Online
{
    public class Listener
    {
        public delegate void Msg(string message);
        public event Msg AddMsg;
        //public delegate void IncomingPacket(Packet pkt);
        //public event IncomingPacket IPacket;
        public Socket Sock;
        Buffer buffer;
        string ip;
        int port;
        MainGame m_MainGame;
        //public bool Conected;

        public Listener(MainGame mg)
        {
            m_MainGame = mg;
            buffer = new Buffer();
            buffer.NewPacket += new Buffer.HNewPacket(incpkt);
        }

        void incpkt(Packet pkt)
        {
            Opcode opc = pkt.GetOpcode();
            switch (opc)
            {
                case Opcode.SM_JOINED:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);
                        string name = br.ReadString();

                        iAddMsg("Игрок " + name + " присоединился.");
                        break;
                    }
                case Opcode.SM_CHAT_MESSAGE:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);
                        string msg = br.ReadString();
                        string name = br.ReadString();
                        m_MainGame.WriteMsgToChat(msg,name);
                        break;
                    }
                case Opcode.SM_SSTEP:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);

                            int x = br.ReadInt16();
                            int y = br.ReadInt16();
                            int xs = br.ReadInt16();
                            int ys = br.ReadInt16();

                            Point xy = new Point(x, y);
                            Point xsys = new Point(xs, ys);

                            if (m_MainGame.dsk.PlayerColor == BaseColor.white)
                                m_MainGame.dsk.InvertStep(ref xy, ref xsys);


                            m_MainGame.Invoke(m_MainGame.dsk.ChangeStepp, xy, xsys);

                            //m_MainGame.dsk.stepp(new Point(x, y), new Point(xs, ys));

                        break;
                    }
                case Opcode.SM_GAME_BLOKING:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);

                        bool x = br.ReadBoolean();

                        m_MainGame.Invoke(m_MainGame.dsk.ChangeGameBl, x);

                        //m_MainGame.paintBox1.Enabled = x; /// проблема с доступностью, злосный баг стопорит игру

                        break;
                    }

                case Opcode.SM_HELLO:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);

                        Int16 x = br.ReadInt16();

                       // m_MainGame.Invoke(m_MainGame.dsk.ChangePlayerNum, (BaseColor)x);

                        m_MainGame.dsk.PlayerColor = (BaseColor)x;

                        break;
                    }

                case Opcode.SM_DIE_C:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);

                        Int16 x = br.ReadInt16();
                        Int16 y = br.ReadInt16();

                        Point xy = new Point(x, y);

                        if (m_MainGame.dsk.PlayerColor == BaseColor.white)
                            m_MainGame.dsk.InvertPoint(ref xy);

                        m_MainGame.dsk.Die_C(xy);

                        break;
                    }
                case Opcode.SM_GAME_DATA:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);
                        Kletka[,] klist = new Kletka[8, 8];
                        int ccx = m_MainGame.dsk.gamewindow_p.X;
                        for (int i = 0; i < 8; i++)
                        {
                            int ccy = m_MainGame.dsk.gamewindow_p.Y;
                            
                            for (int ii = 0; ii < 8; ii++)
                            {
                                klist[i, ii] = new Kletka();
                                klist[i, ii].Color = (BaseColor)br.ReadInt32();
                                bool temp = br.ReadBoolean();
                                if (temp)
                                {
                                    klist[i, ii].Pawn = new Pawn();
                                    klist[i, ii].Pawn.Color = (BaseColor)br.ReadInt32();
                                    klist[i, ii].Pawn.isKing = br.ReadBoolean();
                                }

                                klist[i, ii].coord = new Point(ccx, ccy);
                                ccy += m_MainGame.dsk.KletSize.Y;
                            }
                            ccx += m_MainGame.dsk.KletSize.X;
                            
                        }
                        m_MainGame.dsk.Klet_list = klist;

                        if (m_MainGame.dsk.PlayerColor == BaseColor.white)
                            m_MainGame.dsk.InvertAll();

                        
                        m_MainGame.Invoke(m_MainGame.dsk.CReBuild);
                        m_MainGame.Invoke(m_MainGame.CreatDButton);
                        //m_MainGame.dsk.ReBuild();


                        break;
                    }
                case Opcode.SM_WRONG_MOVE:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);

                        string s = br.ReadString();
                        if (s.Length == 0)
                            s = "Сюда нельзя пойти!";

                        m_MainGame.WriteMsgToChat(s,"Система");
                        m_MainGame.Invoke(m_MainGame.dsk.CReBuild);

                        break;
                    }
                case Opcode.SM_LISTGAMES:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);
                        int count = br.ReadInt32();

                        string[] stti = new string[count];
                        for (int i = 0; i < count; i++)
                        {
                            //stti[i] = ;
                            m_MainGame.Invoke(m_MainGame.startmenu.ListN, br.ReadString());
                        }

                        
                        break;
                    }
                case Opcode.SM_SETKING:
                    {
                        MemoryStream ms = new MemoryStream(pkt.GetData());
                        BinaryReader br = new BinaryReader(ms);
                        Point pp = new Point();
                        pp.X = br.ReadInt32();
                        pp.Y = br.ReadInt32();

                        if (m_MainGame.dsk.PlayerColor == BaseColor.white)
                            m_MainGame.dsk.InvertPoint(ref pp);

                        m_MainGame.dsk.Klet_list[pp.X, pp.Y].Pawn.isKing = true;

                        m_MainGame.Invoke(m_MainGame.dsk.CReBuild);



                        break;
                    }
            }


            //IPacket(pkt);
        }

        public void Connect(string adress, int fport)
        {
            ip = adress;
            port = fport;
            Thread thr = new Thread(new ThreadStart(vc));
            thr.Start();

        }
        void iAddMsg(string msg)
        {
            m_MainGame.Invoke(AddMsg, msg);
        }

        public void TypeSend(Opcode opc, object data)
        {
            switch (opc)
            {
                case Opcode.CM_CREATEROOM:
                    {
                        CMCREATEROOM((string)data);
                        break;
                    }
                case Opcode.CM_JOINROOM:
                    {
                        CMJOINROOM((string)data);
                        break;
                    }
                case Opcode.CM_LISTGAMES:
                    {
                        SendGListRequest();
                        break;
                    }

                    
            }
        }

        public void SendChatMsg(string str)
        {
            DataCreater dc = new DataCreater();

            dc.Write(Opcode.CM_CHAT_MESSAGE);
            dc.Write(str);

            Sock.Send(dc.ToArray());
        }

        public void SendGListRequest()
        {
            DataCreater dc = new DataCreater();

            dc.Write(Opcode.CM_LISTGAMES);
            dc.Write(true);

            Sock.Send(dc.ToArray());
        }

        public void SendSStep(Point p1, Point p2)
        {
            DataCreater dc = new DataCreater();

            if (m_MainGame.dsk.PlayerColor == BaseColor.white)
                m_MainGame.dsk.InvertStep(ref p1, ref p2);

            dc.Write(Opcode.CM_SSTEP);
            dc.Write((Int16)p1.X);
            dc.Write((Int16)p1.Y);
            dc.Write((Int16)p2.X);
            dc.Write((Int16)p2.Y);

            Sock.Send(dc.ToArray());
        }


        void CMCREATEROOM(string name)
        {
            DataCreater dc = new DataCreater();

            dc.Write(Opcode.CM_CREATEROOM);
            dc.Write(name);
            dc.Write(m_MainGame.PlayerName);

            Sock.Send(dc.ToArray());
            iAddMsg("Комната успешно создана.");
        }

        public void CMJOINROOM(string name)
        {
            DataCreater dc = new DataCreater();

            dc.Write(Opcode.CM_JOINROOM);
            dc.Write(name);
            dc.Write(m_MainGame.PlayerName);

            Sock.Send(dc.ToArray());
            iAddMsg("Вы успешно вошли в комнату.");
        }

        void vc()
        {
            TcpClient Client = new TcpClient();
            iAddMsg("Подключение к серверу...");
            try
            {
                Client.Connect(ip, port);
            }
            catch
            {
                iAddMsg("Невозможно подключиться!");
                return;
            }
            Sock = Client.Client;
            iAddMsg("1");
            iAddMsg("Подключение успешно выполнено.");
            while (true)
            {


                //if (!checkBox1.Checked)
                //if (!frist)
                //{
                //    MemoryStream ms = new MemoryStream();
                //    BinaryWriter bw = new BinaryWriter(ms);

                //    bw.Write(4 + 2 + 2);
                //    bw.Write((Int16)3);
                //    bw.Write(new byte[2]);
                //    bw.Flush();

                //    Sock.Send(ms.ToArray());
                //    frist = true;
                //}

                //if (checkBox1.Checked)
                //    if (!frist)
                //    {
                //        MemoryStream ms = new MemoryStream();
                //        BinaryWriter bw = new BinaryWriter(ms);

                //        bw.Write(4 + 2 + 2);
                //        bw.Write((Int16)4);
                //        bw.Write(new byte[2]);
                //        bw.Flush();

                //        Sock.Send(ms.ToArray());
                //        frist = true;
                //    }

                byte[] tt = new byte[1024];
                int ffi;
                try
                {
                    ffi = Sock.Receive(tt);
                }
                catch { break; }
                if (ffi > 0)
                {
                    buffer.AddData(fix(tt, ffi));

                    //MemoryStream ms = new MemoryStream(fix(tt, ffi));
                    //BinaryReader br = new BinaryReader(ms);
                    //int l = br.ReadInt32();
                    //Int16 opc = br.ReadInt16();
                    //if (opc == 2)
                    //{
                    //    int x = br.ReadInt16();
                    //    int y = br.ReadInt16();
                    //    int xs = br.ReadInt16();
                    //    int ys = br.ReadInt16();
                    //    dsk.stepp(new Point(x, y), new Point(xs, ys));
                    //}


                }

            }
            Sock.Close();
            Client.Close();
        }

        byte[] fix(byte[] data, int fix)
        {
            byte[] temp = new byte[fix];
            for (int i = 0; i < fix; i++)
                temp[i] = data[i];
            return temp;

        }
    }
}
