﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Шашки.Online
{
    //class Ocodes
    //{
    //}
    public enum GameStats
    {
        CONECTED,
        PLAYER2STEP,
        ENDGAMEWIN,
        ENDGAMEOVER,
    }

    public enum Opcode
    {
        NULLED,          //не исп
        CM_SSTEP,        //Сообщен серверу данных перемещ шашки.
        SM_SSTEP,        //Сообщ клиенту данных о перемещении шашки.
        CM_CREATEROOM,  //Сообшение серверу, о намерении создать сеанс игры 
        CM_JOINROOM,    //Сообщение серверу, о запросе на поиск доступной игры.
        SM_JOINED,       //Сообщение клиенту 1, о присоединениии клиента 2.
        SM_GAMESTATE,    //Сост игры
        CM_GAMESTATISTIC, //Запрос статистики.
        SM_GAMESTATISTIC, //Ответ статистики.
        CM_ACHIEVEMENTLIST, //Запрос достижений
        SM_ACHIEVEMENTLIST, //Ответ дост.
        CM_PLAYER_INFO,     //Запрос информации о игроке.
        SM_PLAYER_INFO,     //Ответ Инф о игр.
        CM_CHAT_MESSAGE,    //Отправка сообщен серв
        SM_CHAT_MESSAGE,    //отправка сообщ клиенту.
        SM_WRONG_MOVE,      //Неправельный ход.
        SM_GAME_DATA,       //Информация доски.
        SM_GAME_BLOKING,    //Заблокирывать возможности хода.
        SM_HELLO,           //без ко.
        SM_DIE_C,           //Убиваем пешку/дамку
        CM_LISTGAMES,       //Запрос списка игр
        SM_LISTGAMES,       //Ответ со списком игр
        SM_SETKING,         //Пешка становится дамкой
        SM_YOUWIN,          //Вы выиграли
        SM_DEFEAT,          //Вы проиграли
    }
}
