﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Шашки.Online
{
    public class DataCreater : BinaryWriter
    {
    bool opcad;
        public DataCreater() 
            :base( new MemoryStream ())
        {
        }

        public void Write(Opcode value)
        {
            base.Write((Int16)value);
            opcad = true;
        }

        public Packet ToPacket()
        {
            return new Packet(ToArray());
        }

        public byte[] ToArray()
        {
            List<byte> tb = new List<byte>();
            if (opcad)
            {
                byte[] temp = new byte[4];
                temp = BitConverter.GetBytes((Int32)(base.BaseStream.Length + 4));
                tb.AddRange(temp);
                MemoryStream m_ms = (MemoryStream)base.BaseStream;
                tb.AddRange(m_ms.ToArray());
                return tb.ToArray();
            }
            return tb.ToArray();
        }
    }
}
