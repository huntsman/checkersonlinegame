﻿namespace Шашки
{
    partial class MainGame
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chat1 = new Шашки.Chat();
            this.paintBox1 = new Шашки.PaintBox();
            ((System.ComponentModel.ISupportInitialize)(this.paintBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(24, 77);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // chat1
            // 
            this.chat1.Location = new System.Drawing.Point(456, 13);
            this.chat1.Name = "chat1";
            this.chat1.Size = new System.Drawing.Size(264, 330);
            this.chat1.TabIndex = 8;
            // 
            // paintBox1
            // 
            this.paintBox1.Enabled = false;
            this.paintBox1.Location = new System.Drawing.Point(13, 13);
            this.paintBox1.Name = "paintBox1";
            this.paintBox1.Size = new System.Drawing.Size(430, 330);
            this.paintBox1.TabIndex = 0;
            this.paintBox1.TabStop = false;
            this.paintBox1.Visible = false;
            this.paintBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.paintBox1_MouseClick);
            // 
            // MainGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 267);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chat1);
            this.Controls.Add(this.paintBox1);
            this.Name = "MainGame";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.paintBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public PaintBox paintBox1;
        private Chat chat1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

