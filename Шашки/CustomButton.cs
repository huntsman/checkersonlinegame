﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Шашки
{
    public partial class CustomButton : UserControl
    {
        Image Button;
        Image ButtonClick;
        string name;
        string text;


        public CustomButton()
        {
            InitializeComponent();
            Button = Image.FromFile("./Data/ButtonClick.png");
            ButtonClick = Image.FromFile("./Data/Button.png");
            pictureBox1.MouseDown += new MouseEventHandler(pictureBox1_MouseDown);
            pictureBox1.MouseUp +=new MouseEventHandler(pictureBox1_MouseUp);
            PictureBoxLoad();
        }

        public string Name
        {
            set
            {
                name = value;
                label1.Name = name + "Label";
            }
            get
            {
                return name;
            }
        }

        public string Text
        {
            set
            {
                text = value;
                label1.Text = text;
            }
            get
            {
                return name;
            }
        }


        //void LabelLoad()
        //{
        //    label1.Name = name + "Label";
        //    label1.Text = text;
        //}


        void PictureBoxLoad()
        {
            pictureBox1.BackgroundImage = Button;
            pictureBox1.Size = this.Size;
        }

        private void pictureBox1_MouseUp(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = Button;
        }



        private void pictureBox1_MouseDown(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = ButtonClick;
        }

    }
}
