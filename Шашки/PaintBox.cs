﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Шашки
{
    public partial class PaintBox : PictureBox 
    {

        Graphics graph;
        //Timer m_timer;
        Bitmap bm;
        public PaintBox()
        {
            this.InitializeComponent();

            bm = new Bitmap(430, 330);

            // bm.SetResolution(bg.HorizontalResolution, bg.VerticalResolution);
            bm.SetResolution((float)72.0089951, (float)72.0089951);

            graph = Graphics.FromImage(bm);
            //m_timer = new Timer();
            //m_timer.Tick += new System.EventHandler(this.m_timer_Tick);
        }

        //public void StartPainting()
        //{
        //    m_timer.Start();
        //}

        //public void StopPainting()
        //{
        //    m_timer.Stop();
        //}

        private void m_timer_Tick(object sender, EventArgs e)
        {

        }

        public void Clear()
        {
            graph.Clear(Color.White);
            this.Image = bm;
        }

        public void PaintImage(Image img, Point point)
        {

            //Bitmap bmt = new Bitmap(img);
            //if (img.VerticalResolution > 73)
            //{
            //    bmt.SetResolution(72, 72);
            //    img = bmt;
            //}


            graph.DrawImage(img, point.X, point.Y);
            //try
            //{
            this.Image = bm;
            //}
            //catch 
            //{
            //}
        }
    }
}
