﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Шашки.Online;

namespace Шашки
{
    public class StartMenu
    {
        public delegate void msg(string str, int index);
        public event msg MsgTo;
        public sname ListN;
        public delegate void sname(string name);
        List<Button> Buttons;
        List<TextBox> TextBoxs;
        List<Label> Labels;
        public ListBox ListBoxs;
        MainGame m_frm;

        public StartMenu(MainGame frm)
        {
            m_frm = frm;
            ListN = addlistames;
            Buttons = new List<Button>();
            TextBoxs = new List<TextBox>();
            Labels = new List<Label>();
            ListBoxs = new ListBox();
        }

        public void addlistames(string name)
        {
            //for (int i = 0; i < name.Length;i++ )
                ListBoxs.Items.Add(name);
        }

        void Clear()
        {
            for(int i =0; i < Buttons.Count; i++)
                m_frm.Controls.Remove(Buttons[i]);
            Buttons.Clear();
            for (int i = 0; i < TextBoxs.Count; i++)
                m_frm.Controls.Remove(TextBoxs[i]);
            TextBoxs.Clear();
            for (int i = 0; i < Labels.Count; i++)
                m_frm.Controls.Remove(Labels[i]);
            Labels.Clear();

                m_frm.Controls.Remove(ListBoxs);

        }
        void EnableAll()
        {
            for (int i = 0; i < Buttons.Count; i++)
                Buttons[i].Enabled = true;

            for (int i = 0; i < TextBoxs.Count; i++)
                TextBoxs[i].Enabled = true;

            for (int i = 0; i < Labels.Count; i++)
                Labels[i].Enabled = true;

                ListBoxs.Enabled = true;

        }
        void DisableAll()
        {
            for (int i = 0; i < Buttons.Count; i++)
                Buttons[i].Enabled = false;

            for (int i = 0; i < TextBoxs.Count; i++)
                TextBoxs[i].Enabled = false;

            for (int i = 0; i < Labels.Count; i++)
                Labels[i].Enabled = false;

                ListBoxs.Enabled = false;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;
            switch (bt.Name)
            {
                case "SingleGame":
                    {
                        break;
                    }
                case "OnlineGame":
                    {
                        Online();
                        break;
                    }
                case "SettingsGame":
                    {
                        Settings();
                        break;
                    }
                case "ExitGame":
                    {
                        m_frm.Close();
                        break;
                    }
                case "OnlineEnter":
                    {
                        DisableAll();
                        MsgTo(TextBoxs[0].Text, 0); //Инициализируем соединение
                        break;
                    }
                case "CreateGame":
                    {
                        GameCreate();
                        //m_frm.LoadRes();
                        //m_frm.TpcListener.TypeSend(Opcode.CM_CREATEROOM);
                        //Clear();
                        //m_frm.paintBox1.Visible = true;
                        break;
                    }
                case "JoinGame":
                    {
                        GameBrowser();
                        //m_frm.TpcListener.SendGListRequest();
                        m_frm.TpcListener.TypeSend(Opcode.CM_LISTGAMES,null);
                        //m_frm.LoadRes();
                        //m_frm.TpcListener.TypeSend(Opcode.CM_JOINROOM);
                        //Clear();
                        //m_frm.paintBox1.Visible = true;
                        break;
                    }
                case "NickSave":
                    {
                        m_frm.PlayerName = TextBoxs[0].Text;
                        Save();
                        Start();
                        break;
                    }
                case "BrowserBack":
                    {
                        OnlineChoice();
                        break;
                    }
                case "BrowserConnect":
                    {
                        if (ListBoxs.SelectedIndex >= 0)
                        {
                            m_frm.LoadRes();
                            //m_frm.TpcListener.CMJOINROOM(ListBoxs.SelectedItem.ToString());
                            m_frm.TpcListener.TypeSend(Opcode.CM_JOINROOM, ListBoxs.SelectedItem.ToString());
                            Clear();
                            SetNornalGameSize();
                            m_frm.paintBox1.Visible = true;
                        }
                        break;
                    }
                case "CreateNamedGame":
                    {
                        m_frm.LoadRes();
                        m_frm.TpcListener.TypeSend(Opcode.CM_CREATEROOM,TextBoxs[0].Text);
                        Clear();
                        SetNornalGameSize();
                        m_frm.paintBox1.Visible = true;
                        break;
                    }
                case "CreateGBack":
                    {
                        OnlineChoice();
                        break;
                    }
                case "BackGame":
                    {
                        if (m_frm.TpcListener.Sock != null)
                            m_frm.TpcListener.Sock.Close();
                        Start();
                        break;
                    }
                    
                    
                    
            }
        }

        void SetNornalGameSize()
        {
            m_frm.Size = new Size(738, 389);
        }

        void SetMenuGameSize()
        {
            m_frm.Size = new Size(261, 305);
        }

        void GameCreate()
        {
            Clear();
            TextBox tb = new TextBox();
            Button bt = new Button();
            Label lbl = new Label();

            bt.Location = new System.Drawing.Point(88, 100);
            bt.Name = "CreateNamedGame";
            bt.Size = new System.Drawing.Size(75, 23);
            bt.Text = "Создать";
            bt.UseVisualStyleBackColor = true;
            Buttons.Add(bt);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);

            lbl.AutoSize = true;
            lbl.BackColor = System.Drawing.Color.Transparent;
            lbl.Location = new System.Drawing.Point(70, 28);
            lbl.Name = "NameGamel";
            lbl.Size = new System.Drawing.Size(57, 13);
            lbl.Text = "Название:";
            Labels.Add(lbl);
            m_frm.Controls.Add(Labels[0]);

            tb.Location = new System.Drawing.Point(72, 44);
            tb.Name = "NameGameT";
            tb.Size = new System.Drawing.Size(100, 20);
            tb.Text = "Тестовое Название";
            TextBoxs.Add(tb);
            m_frm.Controls.Add(TextBoxs[0]);

            bt = new Button();
            bt.Location = new System.Drawing.Point(88, 129);
            bt.Name = "CreateGBack";
            bt.Size = new System.Drawing.Size(75, 23);
            bt.Text = "Назад";
            bt.UseVisualStyleBackColor = true;
            Buttons.Add(bt);
            Buttons[1].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[1]);
        }

        void GameBrowser()
        {
            Clear();
            ListBoxs = new ListBox();
            Button bt = new Button();
            Label Lab = new Label();
            ListBoxs.FormattingEnabled = true;
            ListBoxs.Location = new System.Drawing.Point(46, 31);
            ListBoxs.Name = "ListGames";
            ListBoxs.Size = new System.Drawing.Size(161, 134);

            m_frm.Controls.Add(ListBoxs);


            bt.Location = new System.Drawing.Point(46, 171);
            bt.Name = "BrowserBack";
            bt.Size = new System.Drawing.Size(75, 23);
            bt.Text = "Назад";
            bt.UseVisualStyleBackColor = true;
            Buttons.Add(bt);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);

            bt = new Button();
            bt.Location = new System.Drawing.Point(129, 171);
            bt.Name = "BrowserConnect";
            bt.Size = new System.Drawing.Size(75, 23);
            bt.Text = "Войти";
            bt.UseVisualStyleBackColor = true;
            Buttons.Add(bt);
            Buttons[1].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[1]);

            Lab.AutoSize = true;
            Lab.BackColor = System.Drawing.Color.Transparent;
            Lab.Location = new System.Drawing.Point(46, 12);
            Lab.Name = "label1";
            Lab.Size = new System.Drawing.Size(151, 13);
            Lab.TabIndex = 11;
            Lab.Text = "Список доступных  сеансов:";
            Labels.Add(Lab);
            m_frm.Controls.Add(Labels[0]);
        }

        void Save()
        {
            FileStream fs = File.OpenWrite("./Settings.st");
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(".st");
            bw.Write(m_frm.PlayerName);
            bw.Flush();
            bw.Dispose();
            fs.Dispose();

        }

        public void OnlineChoice()
        {
            Clear();
            Button btn;
            //button 0
            btn = new Button();
            btn.Name = "CreateGame";
            btn.Text = "Создать";
            btn.Size = new System.Drawing.Size(77, 23);
            btn.Location = new System.Drawing.Point(83, 50);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);
            //button 2
            btn = new Button();
            btn.Name = "JoinGame";
            btn.Text = "Присоедениться";
            btn.Size = new System.Drawing.Size(77, 23);
            btn.Location = new System.Drawing.Point(83, 100);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[1].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[1]);
            //button 3
            btn = new Button();
            btn.Name = "BackGame";
            btn.Text = "Назад";
            btn.Size = new System.Drawing.Size(77, 23);
            btn.Location = new System.Drawing.Point(83, 150);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[2].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[2]);
        }

        void Online()
        {
            Clear();
            Button btn;
            TextBox tb;
            Label Lbl;
            //Label 0
            Lbl = new Label();
            Lbl.Name = "ips";
            Lbl.BackColor = System.Drawing.Color.Transparent;
            Lbl.Text = "Адресс сервера:";
            Lbl.Size = new System.Drawing.Size(92, 13);
            Lbl.Location = new System.Drawing.Point(58, 30);
            Labels.Add(Lbl);
            m_frm.Controls.Add(Labels[0]);
            //Textbox 0
            tb = new TextBox();
            tb.Name = "IpAdress";
            tb.Text = "127.0.0.1";
            tb.Size = new System.Drawing.Size(120, 20);
            tb.Location = new System.Drawing.Point(58, 50);
            TextBoxs.Add(tb);
            m_frm.Controls.Add(TextBoxs[0]);
            //button 0
            btn = new Button();
            btn.Name = "OnlineEnter";
            btn.Text = "Войти";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 100);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);
        }

        void Settings()
        {
            Clear();
            Button btn;
            TextBox tb;
            Label Lbl;
            //Label 0
            Lbl = new Label();
            Lbl.Name = "lNick";
            Lbl.BackColor = System.Drawing.Color.Transparent;
            Lbl.Text = "Ник в игре:";
            Lbl.Size = new System.Drawing.Size(92, 13);
            Lbl.Location = new System.Drawing.Point(58, 30);
            Labels.Add(Lbl);
            m_frm.Controls.Add(Labels[0]);
            //Textbox 0
            tb = new TextBox();
            tb.Name = "NickName";
            tb.Text = "Player";
            tb.Size = new System.Drawing.Size(120, 20);
            tb.Location = new System.Drawing.Point(58, 50);
            TextBoxs.Add(tb);
            m_frm.Controls.Add(TextBoxs[0]);
            //button 0
            btn = new Button();
            btn.Name = "NickSave";
            btn.Text = "Сохранить";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 100);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);
        }

        public void Start()
        {
            Clear();
            Button btn;
            //button 0
            btn = new Button();
            btn.Name = "SingleGame";
            btn.Text = "Одиночка";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 50);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[0].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[0]);
            //button 1
            btn = new Button();
            btn.Name = "OnlineGame";
            btn.Text = "Онлайн";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 100);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[1].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[1]);
            //button 2
            btn = new Button();
            btn.Name = "SettingsGame";
            btn.Text = "Настройки";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 150);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[2].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[2]);
            //button 3
            btn = new Button();
            btn.Name = "ExitGame";
            btn.Text = "Выход";
            btn.Size = new System.Drawing.Size(75, 23);
            btn.Location = new System.Drawing.Point(83, 200);
            btn.UseVisualStyleBackColor = true;
            Buttons.Add(btn);
            Buttons[3].Click += new EventHandler(Button_Click);
            m_frm.Controls.Add(Buttons[3]);

        }


    }
}
