﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Server
{
    public class Packet
    {
        Opcode m_pcode;
        byte[] m_data;
        public Packet(Opcode Opcode, byte[] Data)
        {
            m_pcode = Opcode;
            m_data = Data;
        }

        public Packet(byte[] Data)
        {
            m_data = Data;
        }

        public Opcode GetOpcode() { return m_pcode; }
        public byte[] GetData() { return m_data; }
        public byte[] ToArray()
        {
            int Length = 4 + 2 + m_data.Length;
            //byte[] temp = new byte[4 + 2 + m_data.Length];
            MemoryStream ms = new MemoryStream();
            BinaryWriter br = new BinaryWriter(ms);
            if (m_pcode != 0)
            {
                br.Write(Length);
                br.Write((Int16)m_pcode);
            }
            br.Write(m_data);
            br.Flush();

            return ms.ToArray();
        }
        //public void 
    }
}
