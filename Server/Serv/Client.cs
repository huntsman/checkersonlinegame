﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using Server.Game;

namespace Server
{
    class Client
    {
        Socket m_client;
        Buffer m_buffer;
        //Польз
        string Name;


        bool inGameRoom;
        //bool CreaterRoom;
        int GameRoomId;
        public Client(Socket Client)
        {
            m_client = Client;
            m_buffer = new Buffer();
            m_buffer.NewPacket += new Buffer.HNewPacket(this.IncomingPacket);
        }

        byte[] fix(byte[] data, int fix)
        {
            byte[] temp = new byte[fix];
            for (int i = 0; i < fix; i++)
                temp[i] = data[i];
            return temp;

        }

        public void ReadData()
        {
            byte[] bt = new byte[1024];
            int i = 0;
            while (true)
            {


                //if (m_client.Available > 0)
                //{
                //    bt = new byte[m_client.Available];
                try
                {
                    i = m_client.Receive(bt);
                }
                catch
                {
                    m_client.Close();
                    Console.WriteLine("Игрок " + Name + " отключился.");
                    if (Game.Game.GameRooms.Count > 0)
                    if (!Game.Game.GameRooms[GameRoomId].gamefull)
                    {
                        Game.Game.GameRooms.RemoveAt(GameRoomId);
                        Console.WriteLine("Игровая комната " + GameRoomId + " удалена.");
                    }
                    else
                    Game.Game.GameRooms[GameRoomId].gamefull = false;
                    break;
                }
                    if (i > 0)
                    {
                        m_buffer.AddData(fix(bt, i));
                        //if (i > bt.Length || i < bt.Length)
                        //    Console.WriteLine("Внимание: Ошибка в блоке чтения потока!!!");
                    }
                //}
            }
        }


        void SelfSend(Packet pkt)
        {
            byte[] temp = pkt.ToArray();
                m_client.Send(temp);
        }

        void OpcodeCMJOINMATCH(Packet pkt)
        {
            bool newpl;
            MemoryStream ms = new MemoryStream(pkt.GetData());
            BinaryReader br = new BinaryReader(ms);
            string gamename = br.ReadString();
            Name = br.ReadString();
            br.Dispose(); ms.Dispose();

            if (!inGameRoom)
            {
                for (int i = 0; i < Game.Game.GameRooms.Count; i++)
                {
                    if (Game.Game.GameRooms[i].Name == gamename)
                    {
                        GameRoomId = i;
                        break;
                    }
                    if (i == Game.Game.GameRooms.Count - 1)
                        return;
                }

                //for (int i = 0; i < Game.Game.GameRooms.Count; i++)
                //{
                //    if (!Game.Game.GameRooms[i].gamefull)
                //    {
                //        GameRoomId = i;
                //        break;
                //    }
                //}
                newpl = Game.Game.GameRooms[GameRoomId].Player1.Connected;

                Game.Game.GameRooms[GameRoomId].AddPlayer(m_client);
                //Пакет
                DataCreater dc = new DataCreater();
                dc.Write(Opcode.SM_JOINED);
                dc.Write(Name);
                BaseColor tmp;
                if (newpl)
                    tmp = BaseColor.white;
                else
                    tmp = BaseColor.black;

                Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), tmp);

                inGameRoom = true;
                
                Console.WriteLine("Игрок " + Name + " вошёл в игровую комнату " + GameRoomId);
                //bool nn = !newpl;
                SendHello(ColorInertor.InvertColor(tmp));
                SendData(ColorInertor.InvertColor(tmp));
                SendBlock(false);
            }
        }

        void SendHello(BaseColor color)
        {
            DataCreater dc = new DataCreater();
            dc.Write(Opcode.SM_HELLO);
            dc.Write((Int16)color);

            Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), color);
        }

        void SendDiec(Point checker)
        {
            DataCreater dc = new DataCreater();
            dc.Write(Opcode.SM_DIE_C);
            //bool ni6;
            //if (Game.Game.GameRooms[GameRoomId].DsK.Klet_list[checker.X, checker.Y].PlayerNumber == 1) ni6 = true; else ni6 = false;
            dc.Write((Int16)checker.X);
            dc.Write((Int16)checker.Y);

            Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), Game.Game.GameRooms[GameRoomId].DsK.Klet_list[checker.X, checker.Y].Pawn.Color);
        }

        void SendBlock(bool smn)
        {
            //Смена хода
            if (smn)
                if (Game.Game.GameRooms[GameRoomId].PlayerColor == BaseColor.white) Game.Game.GameRooms[GameRoomId].PlayerColor = BaseColor.black;
                else
                    Game.Game.GameRooms[GameRoomId].PlayerColor = BaseColor.white;


            //bool ni6;
            //if (Game.Game.GameRooms[GameRoomId].PlayerColor == BaseColor.white) ni6 = true; else ni6 = false;

            DataCreater dc = new DataCreater();
            BaseColor temp = Game.Game.GameRooms[GameRoomId].PlayerColor;



                dc.Write(Opcode.SM_GAME_BLOKING);
                dc.Write(true);
                Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), temp);
                SystemSendIC("Ваш ход!", temp);




                dc = new DataCreater();
                dc.Write(Opcode.SM_GAME_BLOKING);
                dc.Write(false);
                Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), ColorInertor.InvertColor(temp));
                SystemSendIC("Противник делает ход.", ColorInertor.InvertColor(temp));




        }

        void SendData(BaseColor color)
        {
            DataCreater dc = new DataCreater();
            dc.Write(Opcode.SM_GAME_DATA);
            for (int i = 0; i < 8; i++)
            {
                for (int ii = 0; ii < 8; ii++)
                {
                    dc.Write((int)Game.Game.GameRooms[GameRoomId].DsK.Klet_list[i, ii].Color);
                    if (Game.Game.GameRooms[GameRoomId].DsK.Klet_list[i, ii].Pawn != null)
                    {
                        dc.Write(true);
                        dc.Write((int)Game.Game.GameRooms[GameRoomId].DsK.Klet_list[i, ii].Pawn.Color);
                        dc.Write(Game.Game.GameRooms[GameRoomId].DsK.Klet_list[i, ii].Pawn.isKing);
                    }
                    else
                        dc.Write(false);
                }
            }
            Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), color);
        }

        void SendInChat(string msg, BaseColor Color)
        {
            if (msg.Length > 0)
            {
                //Пакет
                DataCreater dc = new DataCreater();
                dc.Write(Opcode.SM_CHAT_MESSAGE);
                dc.Write(msg);
                dc.Write(Name);

                Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), Color);
            }

        }

        void SystemSendIC(string msg, BaseColor Color)
        {
            if (msg.Length > 0)
            {
                //Пакет
                DataCreater dc = new DataCreater();
                dc.Write(Opcode.SM_CHAT_MESSAGE);
                dc.Write(msg);
                dc.Write("Система");

                Game.Game.GameRooms[GameRoomId].Send(dc.ToPacket(), Color);
            }

        }



        void OpcodeMCREATE(Packet pkt)
        {
            MemoryStream ms = new MemoryStream(pkt.GetData());
            BinaryReader br = new BinaryReader(ms);
            string srtname = br.ReadString();
            Name = br.ReadString();
            br.Dispose(); ms.Dispose();
            if (!inGameRoom)
            {
                Game.Game.GameRooms.Add(new GameRoom(m_client));
                GameRoomId = Game.Game.GameRooms.Count - 1;
                Game.Game.GameRooms[GameRoomId].Name = srtname;
                inGameRoom = true;
                //CreaterRoom = true;
                SendHello(BaseColor.white);
                Console.WriteLine("Игроком " + Name + " была создана комната.");
                SendData(BaseColor.white);
            }
        }

        bool SPlayerI()
        {
            if (Game.Game.GameRooms[GameRoomId].PlayerColor == BaseColor.white) return true; else return false;
        }

        void OpcodeCMSSTEP(Packet pkt)
        {
            MemoryStream ms = new MemoryStream(pkt.GetData());
            BinaryReader br = new BinaryReader(ms);

            Int16 X = br.ReadInt16();
            Int16 Y = br.ReadInt16();

            Int16 XS = br.ReadInt16();
            Int16 YS = br.ReadInt16();

            //DataCreater dc = new DataCreater();
            //dc.Write(Opcode.SM_SSTEP);
            //dc.Write(X);
            //dc.Write(Y);
            //dc.Write(XS);
            //dc.Write(YS);
            //Game.Game.GameRooms[GameRoomId].SendAll(dc.ToPacket());

            bool b2 = Game.Game.GameRooms[GameRoomId].DsK.stepp(new Point(X, Y), new Point(XS, YS), Game.Game.GameRooms[GameRoomId].PlayerColor);

            if (b2 == true)
            {

                Console.WriteLine("Игрок " + Name + " сделал ход.");
                bool bb = false;
                //if (b2.boolt)
                //    bb = Game.Game.GameRooms[GameRoomId].DsK.bkenemy(new Point(XS, YS));
                //if (!bb)
                SendBlock(true);
            }

        }


        void OpcodeCMLISTGAMES(Packet pkt)
        {
            DataCreater dc = new DataCreater();
            dc.Write(Opcode.SM_LISTGAMES);
            dc.Write(Game.Game.GameRooms.Count);
            for (int i = 0; i < Game.Game.GameRooms.Count; i++)
            {
                dc.Write(Game.Game.GameRooms[i].Name);
            }


            m_client.Send(dc.ToPacket().ToArray());
        }

        void OpcodeCMCHATMESSAGE(Packet pkt)
        {
            MemoryStream ms = new MemoryStream(pkt.GetData());
            BinaryReader br = new BinaryReader(ms);
            string msg = br.ReadString();
            if (msg.Length > 0)
            {
                //Пакет
                DataCreater dc = new DataCreater();
                dc.Write(Opcode.SM_CHAT_MESSAGE);
                dc.Write(msg);
                dc.Write(Name);

                Game.Game.GameRooms[GameRoomId].SendAll(dc.ToPacket());
            }
            br.Dispose(); ms.Dispose();

        }


        public void IncomingPacket(Packet pkt)
        {
            Opcode opc = pkt.GetOpcode();
            switch (opc)
            {
                case Opcode.CM_CREATEROOM:
                    {
                        OpcodeMCREATE(pkt);
                        break;
                    }
                case Opcode.CM_JOINROOM:
                    {
                        OpcodeCMJOINMATCH(pkt);
                        break;
                    }
                case Opcode.CM_SSTEP:
                    {
                        OpcodeCMSSTEP(pkt);
                        break;
                    }
                case Opcode.CM_CHAT_MESSAGE:
                    {
                        OpcodeCMCHATMESSAGE(pkt);
                        break;
                    }
                case Opcode.CM_LISTGAMES:
                    {
                        OpcodeCMLISTGAMES(pkt);
                        break;
                    }

            }
        }
    }
}
