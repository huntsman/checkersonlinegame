﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Server
{
    class SererListener
    {
            private IPAddress LocalAdr;
            private int LocalPort;
            private Thread ServThread; // экземпляр потока
            TcpListener Listener; // листенер))))
            
            public void Create(string IPAdress, int port)
            {
                Console.WriteLine("Сервер начинает работу.");
                LocalAdr = IPAddress.Parse(IPAdress);
                LocalPort = port;
                ServThread = new Thread(new ThreadStart(ServStart));
                ServThread.Start(); // запустили поток. Стартовая функция – 
                                    // ServStart, как видно выше
            }
            
            public void Close() // Закрыть серв?
            {
                Console.WriteLine("Сервер завершает работу.");
                Listener.Stop();
                ServThread.Abort();
                return;
            }

            private void newclient(Object Client)
            {
                Client client;
                Socket cl = (Socket)Client;
                if (cl.Connected)
                {
                    client = new Client(cl);
                    Console.WriteLine("Клиент подключен, пошла передача)))");
                    client.ReadData();
                }
            }
        
            private void ServStart()
            {
                Socket ClientSock; // сокет для обмена данными.
                Thread ClientThread;
                Listener = new TcpListener(LocalAdr,LocalPort); 
                Listener.Start(); // начали слушать
                Console.WriteLine("Waiting connections [" + LocalAdr + ":" + Convert.ToString(LocalPort) + "]...");
                while (true)
                {
                    //if (Listener.Server.Connected)
                    //{
                        try
                        {
                            ClientSock = Listener.AcceptSocket(); // пробуем принять 
                            Console.WriteLine("Подключение клиента:" + ClientSock.LocalEndPoint.ToString());
                            // клиента
                            ClientThread = new Thread(new ParameterizedThreadStart(newclient));
                            ClientThread.Start(ClientSock);
                        }
                        catch
                        {
                            Console.WriteLine("Ошибка подключения.:(");
                            ServThread.Abort(); // нет – жаль(
                            return;
                        }
                        //int i = 0;

                        //if (ClientSock.Connected)
                        //{
                        //    while (true)
                        //    {
                        //        try
                        //        {
                        //            i = ClientSock.Receive(cldata); // попытка чтения 
                        //            // данных
                        //        }
                        //        catch
                        //        {
                        //        }

                        //        try
                        //        {
                        //            if (i > 0)
                        //            {

                        //                data = Encoding.ASCII.GetString(cldata).Trim();
                        //                Console.WriteLine("<" + data);
                        //                if (data == "CLOSE") // если CLOSE – 
                        //                // вырубимся
                        //                {
                        //                    ClientSock.Send(Encoding.ASCII.GetBytes("Closing the server..."));
                        //                    ClientSock.Close();
                        //                    Listener.Stop();
                        //                    Console.WriteLine("Server closed. Reason: client wish! Type EXIT to quit the application.");
                        //                    ServThread.Abort();
                        //                    return;
                        //                }
                        //                else
                        //                { // нет – шлем данные взад.
                        //                    ClientSock.Send(Encoding.ASCII.GetBytes("Your data: " + data));
                        //                }
                        //            }
                        //        }
                        //        catch
                        //        {
                        //            ClientSock.Close(); // ну эт если какая хрень..
                        //            Listener.Stop();
                        //            Console.WriteLine("Server closing. Reason: client offline. Type EXIT to quit the application.");
                        //            ServThread.Abort();
                        //        }
                        //    }
                        //}
                    //}
                }
            }
    }
}
