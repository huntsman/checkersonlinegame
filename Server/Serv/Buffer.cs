﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Server
{
    class Buffer // для правильной работы размер пакета должен быть указан вместе с 4 байтами размера Int32
    {
        public delegate void HNewPacket(Packet Packet);
        public event HNewPacket NewPacket;
        List<byte> buffer;
        int LastSize;
        public Buffer()
        {
            buffer = new List<byte>();
        }

        void npacket(byte[] data)
        {
            Packet pkt;
            MemoryStream ms = new MemoryStream(data);
            BinaryReader br = new BinaryReader(ms);
            br.ReadInt32();
            Opcode opc = (Opcode)br.ReadInt16();
            byte[] tmp = br.ReadBytes(data.Length - (int)(br.BaseStream.Position));
            pkt = new Packet(opc, tmp);
            NewPacket(pkt);
        }

        public void AddData(byte[] data)
        {
            bool frist = true;
            while (true)
            {
                if (frist)
                    buffer.AddRange(data);

                //if (buffer.Count == 0)
                //{
                MemoryStream ms = new MemoryStream(buffer.ToArray());
                BinaryReader br = new BinaryReader(ms);
                LastSize = br.ReadInt32();
                br.Dispose();
                ms.Dispose();
                //}



                if (buffer.Count == LastSize)
                {
                    LastSize = 0;
                    npacket(buffer.ToArray());
                    buffer.Clear();
                    return;
                }

                if (buffer.Count < LastSize)
                    return;

                byte[] temp = new byte[LastSize];
                for (int i = 0; i < LastSize; i++)
                    temp[i] = buffer[i];
                buffer.RemoveRange(0, LastSize);
                npacket(temp);
                temp = new byte[1];
                if (buffer.Count == 0)
                    return;
                frist = false;
            }
        }
    }
}
