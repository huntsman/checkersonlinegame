﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Server.Game
{
    public class GameRoom
    {
        public string Name;
        public Socket Player1;
        public Socket Player2;
        public bool gamefull;
        public BaseColor PlayerColor; // Цвет игрока которому доступен ход
        public Doska DsK;


        public GameRoom(Socket player)
        {
            DsK = new Doska(this);
            PlayerColor = BaseColor.white;
            Player1 = player;
            DsK.BuildGame();
        }

        #region SockOperation

        public void AddPlayer(Socket player)
        {
            if(Player1.Connected)
            Player2 = player;
            else
            Player1 = player;
            gamefull = true;
        }

        public void SendAll(Packet pkt)
        {
            byte[] temp = pkt.ToArray();
            Player1.Send(temp);
            Player2.Send(temp);
        }

        public void Send(Packet pkt, BaseColor Color)
        {
            byte[] temp = pkt.ToArray();
            if (Color == BaseColor.white) 
            Player1.Send(temp);
            else
            Player2.Send(temp);
        }
        #endregion
    }
}
